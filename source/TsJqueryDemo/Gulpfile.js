﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var copy = require('gulp-copy');
var concat = require('gulp-concat');
gulp.task('libs', function () {
    gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery/dist/jquery.min.map',
        'bower_components/moment/moment.js',
        'bower_components/requirejs-plugins/lib/require.js'
    ]).pipe(gulp.dest('scripts/libs/'));
});
gulp.task('admin', function () {
    //concat javascript files
    gulp.src([
        'scripts/app/*.js'
    ]).pipe(concat('app.min.js')).pipe(gulp.dest('scripts/app/'));
});
gulp.task('all', ['libs', 'admin'], function () {

});