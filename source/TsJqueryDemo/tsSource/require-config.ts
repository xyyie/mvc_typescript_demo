﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts"/>
/// <reference path="../scripts/typings/requirejs/require.d.ts"/>
require.config({
    baseUrl: '/scripts/App/',
    paths: {
        'jquery': '/bower_components/jquery/dist/jquery',
        'easyui': '/scripts/libs/easyui/jquery.easyui.min'
    },

    shim: {
        jquery: {
            exports: '$'
        },
        'easyui': ['jquery']
    }
});
