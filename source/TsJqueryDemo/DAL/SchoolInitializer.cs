﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TsJqueryDemo.Models;

namespace TsJqueryDemo.DAL
{
    public class SchoolInitializer: System.Data.Entity.DropCreateDatabaseAlways<SchoolContext>
    {
        protected override void Seed(SchoolContext context)
        {
            var students = new List<Student>()
            {
                new Student() { FirstMidName="feng", LastName="yz", EnrollmentDate=DateTime.Parse("2017-01-01") },
                new Student() { FirstMidName="li", LastName="f", EnrollmentDate=DateTime.Parse("2017-01-02") }
            };
            students.ForEach(s => context.Students.Add(s));
            context.SaveChanges();

            var courses = new List<Course>()
            {
                new Course() { CourseID=1001,Title="English", Credits=3 },
                new Course() {CourseID=1002,Title="Math",Credits=4 }
            };
            courses.ForEach(c => context.Courses.Add(c));
            context.SaveChanges();

            //var enrollments = new List<Enrollment>()
            //{
            //    new Enrollment() {StudentID=students[0].ID, CourseID=courses[0].CourseID, Grade=Grade.A },
            //    new Enrollment() {StudentID=students[0].ID, CourseID=courses[1].CourseID ,Grade=Grade.B},
            //    new Enrollment() {StudentID=students[0].ID, CourseID=courses[2].CourseID ,Grade=Grade.C}
            //};
        }
    }
}