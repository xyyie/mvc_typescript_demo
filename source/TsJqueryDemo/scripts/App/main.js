/// <reference path="../scripts/typings/jquery/jquery.d.ts"/>
define(["require", "exports", "./user", "jquery"], function (require, exports, user, $) {
    "use strict";
    var u = new user.User();
    $(document).ready(function () {
        u.login();
    });
});
//# sourceMappingURL=main.js.map