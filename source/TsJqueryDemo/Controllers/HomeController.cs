﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TsJqueryDemo.DAL;

namespace TsJqueryDemo.Controllers
{
    public class HomeController : Controller
    {
        private SchoolContext db = new SchoolContext();
        // GET: Home
        public ActionResult Index()
        {
            return View(db.Students.ToList());
        }
    }
}